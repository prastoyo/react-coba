import React from 'react'
import './HelloComponent.css'

const HelloComponent = () => {
    return <p class='text-p'> Hello Functional Component </p>
}

export default HelloComponent